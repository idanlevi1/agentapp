import {Component, OnInit} from '@angular/core';
import {AgentsSandboxService} from '../agents-sandbox.service';

@Component({
  selector: 'app-agent-list',
  templateUrl: './agent-list.component.html',
  styleUrls: ['./agent-list.component.sass']
})
export class AgentListComponent implements OnInit {

  constructor(public agentSandbox: AgentsSandboxService) {
  }

  ngOnInit() {
    this.agentSandbox.fetchAgentList();
  }
}
