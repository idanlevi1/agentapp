import {Injectable} from '@angular/core';
import {AgentsProviderService} from './agents-provider.service';
import {Store} from '@ngrx/store';
import * as fromAgents from '../store/agent/agents.reducer';
import {Observable} from 'rxjs';
import {Agent} from './agent/agent.model';
import {AgentActionsTypes} from '../store/agent/agents.actions';
import {AgentUtils} from '../../utils/agent-utils';
@Injectable()
export class AgentsSandboxService {
  public agentsState$: Observable<Agent[]> = this.store.select((state) => AgentUtils.agentsSelector(state));

  constructor(private store: Store<fromAgents.State>,
              private agentProvider: AgentsProviderService) {
  }

  public fetchAgentList(): void {
    this.agentProvider.fetchDB()
      .subscribe(
        (agentList: Agent[]) => {
          this.setAgentsToStore(agentList);
        }
      );
  }

  private setAgentsToStore(agentsData: Agent[]): void {
    this.store.dispatch({
        type: AgentActionsTypes.setAgents, payload: agentsData
      }
    );
  }

  public setSearchToStore(searchData: string): void {
    this.store.dispatch({
        type: AgentActionsTypes.setSearch, payload: searchData
      }
    );
  }

  public updateLocationToStore(newLocation: string): void {
    this.store.dispatch({
        type: AgentActionsTypes.filterByLocation, payload: newLocation
      }
    );
  }

  public updateRatingToStore(): void {
    this.store.dispatch({
        type: AgentActionsTypes.sortByRating, payload: true
      }
    );
  }

  public updateNumberOfRatersToStore(): void {
    this.store.dispatch({
        type: AgentActionsTypes.sortByNumberOfRaters, payload: true
      }
    );
  }

  public updateSortByPriceToStore(option: string): void {
    this.store.dispatch({
        type: AgentActionsTypes.sortByPrice, payload: option
      }
    );
  }


}
