import { Component, OnInit } from '@angular/core';
import {AgentsSandboxService} from '../agents-sandbox.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.sass']
})
export class FiltersComponent implements OnInit {

  constructor(private agentSandbox: AgentsSandboxService) { }

  ngOnInit() {
  }

  selectSortByPrice(option) {
    this.agentSandbox.updateSortByPriceToStore(option.target.value);
  }

  selectSortByLocation(option) {
    console.log(option.target.value);
    this.agentSandbox.updateLocationToStore(option.target.value);
  }

  selectByRating() {
    this.agentSandbox.updateRatingToStore();
  }

  selectByNumberOfRaters() {
    this.agentSandbox.updateNumberOfRatersToStore();
  }
}
