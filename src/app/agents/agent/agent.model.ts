import {Location} from './location.model';

export class Agent {
  constructor(
    public name: string,
    public description: string,
    public imagePath: string,
    public location: Location,
    public price: number,
    public rating: number,
    public numberOfRaters: number,
  ) {
  }
}

