import {Component, Input, OnInit} from '@angular/core';
import {Agent} from './agent.model';

@Component({
  selector: 'app-agent',
  templateUrl: './agent.component.html',
  styleUrls: ['./agent.component.sass']
})
export class AgentComponent implements OnInit {
  @Input() agent: Agent;

  constructor() {
  }

  ngOnInit() {

  }

  counter(i: number) {
    return new Array(i);
  }
}
