import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Agent} from './agent/agent.model';
import {map} from 'rxjs/operators';
import {plainToClass} from 'class-transformer';

@Injectable()
export class AgentsProviderService {
  readonly DB_URL = '/assets/api/DB.json';

  constructor(private  httpClient: HttpClient) {
  }

  fetchDB(): Observable<Agent[]> {
    return this.httpClient.get(this.DB_URL)
      .pipe(map((res: Response) => plainToClass(Agent, res['agents'])));
  }
}
