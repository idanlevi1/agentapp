import {Action} from '@ngrx/store';
import {Agent} from '../../agents/agent/agent.model';

export const SET_AGENTS = 'SET_AGENTS';
export const SET_SEARCH = 'SET_SEARCH';
export const FILTER_BY_LOCATION = 'FILTER_BY_LOCATION';
export const SORT_BY_RATING = 'SORT_BY_RATING';
export const SORT_BY_NUMBER_OF_RATERS = 'SORT_BY_NUMBER_OF_RATERS';
export const SORT_BY_PRICE = 'SORT_BY_PRICE';

// numberOfRaters
export enum AgentActionsTypes {
  setAgents = 'SET_AGENTS',
  setSearch = 'SET_SEARCH',
  filterByLocation = 'FILTER_BY_LOCATION',
  sortByRating = 'SORT_BY_RATING',
  sortByNumberOfRaters = 'SORT_BY_NUMBER_OF_RATERS',
  sortByPrice = 'SORT_BY_PRICE',
}

export class SetAgents implements Action {
  readonly type = SET_AGENTS;

  constructor(public payload: Agent[]) {
  }
}

export class SetSearch implements Action {
  readonly type = SET_SEARCH;

  constructor(public payload: string) {
  }
}

export class FilterByLocation implements Action {
  readonly type = FILTER_BY_LOCATION;

  constructor(public payload: string) {
  }
}

export class SortByRating implements Action {
  readonly type = SORT_BY_RATING;

  constructor(public payload: string) {
  }
}

export class SortByNumberOfRaters implements Action {
  readonly type = SORT_BY_NUMBER_OF_RATERS;

  constructor(public payload: boolean) {
  }
}

export class SortByPrice implements Action {
  readonly type = SORT_BY_PRICE;

  constructor(public payload: string) {
  }
}

export type AgentActions = SetAgents |
  SetSearch |
  FilterByLocation |
  SortByRating |
  SortByNumberOfRaters |
  SortByPrice;
