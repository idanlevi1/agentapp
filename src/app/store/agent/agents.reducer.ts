import {Agent} from '../../agents/agent/agent.model';
import * as AgentActions from './agents.actions';

export interface State {
  agents: Agent[];
  filters: {
    search: string;
    location: string;
    rating: boolean;
    numberOfRaters: boolean;
    priceDownToUp: boolean
  };
}

const initialState: State = {
  agents: [],
  filters: {
    search: '',
    location: '',
    rating: false,
    numberOfRaters: false,
    priceDownToUp: true
  }
};

export function agentsReducer(state = initialState, action: AgentActions.AgentActions) {
  console.log('redux:', action.type, state);
  switch (action.type) {
    case (AgentActions.SET_AGENTS):
      return {
        ...state,
        agents: [...action.payload],
      };
    case (AgentActions.SET_SEARCH):
      return {
        ...state,
        filters: {
          ...state.filters,
          search: action.payload
        }
      };
    case (AgentActions.FILTER_BY_LOCATION):
      return {
        ...state,
        filters: {
          ...state.filters,
          location: action.payload
        }
      };
    case (AgentActions.SORT_BY_RATING):
      return {
        ...state,
        filters: {
          ...state.filters,
          rating: action.payload,
          numberOfRaters: !action.payload
        }
      };
    case (AgentActions.SORT_BY_NUMBER_OF_RATERS):
      return {
        ...state,
        filters: {
          ...state.filters,
          numberOfRaters: action.payload,
          rating: !action.payload
        }
      };
    case (AgentActions.SORT_BY_PRICE):
      return {
        ...state,
        filters: {
          ...state.filters,
          priceDownToUp: action.payload === 'downToUp' ? true : false
        }
      };
    default:
      return state;
  }
}

