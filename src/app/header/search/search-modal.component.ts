import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AgentsSandboxService} from '../../agents/agents-sandbox.service';
import {searchModalAnimation} from './search-modal.animation';

@Component({
  selector: 'app-search-modal',
  templateUrl: './search-modal.component.html',
  styleUrls: ['./search-modal.component.sass'],
  animations: searchModalAnimation
})
export class SearchModalComponent implements OnInit {
  searchForm: FormGroup;
  @Output() searchClicked: EventEmitter<void> = new EventEmitter<void>();
  @Input() modalState;

  constructor(private agentsSandBoxService: AgentsSandboxService) {
  }

  ngOnInit() {
    this.searchForm = new FormGroup({
      'search': new FormControl(null)
    });
  }

  onSearch() {
    this.agentsSandBoxService.setSearchToStore(this.searchForm.value.search);
    this.searchClicked.emit();
    this.modalState = 'open';
  }
}
