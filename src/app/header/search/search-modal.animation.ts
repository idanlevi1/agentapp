import {animate, state, style, transition, trigger} from '@angular/animations';

export const searchModalAnimation = [
  trigger('searchModal', [
    state('close', style({
      display: 'none',
      opacity: 0,
      transform: 'scale(0)',
      backgroundColor: '#00d3e6'
    })),
    state('open', style({
      opacity: 1,
      display: 'block'
    })),
    transition('open => close', [
      animate('.35s')
    ]),
    transition('close => open', [
      animate('.35s')
    ]),
  ])
];
