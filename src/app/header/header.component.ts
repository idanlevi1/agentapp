import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass'],
})
export class HeaderComponent implements OnInit {
  searchModal = false;
  modalState = 'close';

  constructor() {
  }

  ngOnInit() {
  }

  toggleSearchModal() {
    this.modalState = this.modalState === 'close' ? 'open' : 'close';
  }

}
