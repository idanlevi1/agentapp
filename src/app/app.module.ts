import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {agentsReducer} from './store/agent/agents.reducer';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { AgentsComponent } from './agents/agents.component';
import { AgentListComponent } from './agents/agent-list/agent-list.component';
import { SearchComponent } from './agents/search/search.component';
import {AgentsProviderService} from './agents/agents-provider.service';
import {AgentsSandboxService} from './agents/agents-sandbox.service';
import { AgentComponent } from './agents/agent/agent.component';
import {SearchModalComponent} from './header/search/search-modal.component';
import { FiltersComponent } from './agents/filters/filters.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    AgentsComponent,
    AgentListComponent,
    SearchComponent,
    SearchModalComponent,
    AgentComponent,
    FiltersComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    StoreModule.forRoot({agentsState: agentsReducer})
  ],
  providers: [AgentsProviderService, AgentsSandboxService],
  bootstrap: [AppComponent]
})
export class AppModule { }
