import {Agent} from '../app/agents/agent/agent.model';
import * as fromAgents from '../app/store/agent/agents.reducer';
import {groupBy} from 'lodash';
import {StringUtils} from './string-utils';

export class AgentUtils {
  public static agentsSelector(state: fromAgents.State): Agent[] {
    const agentsStateResult = {
      agents: state['agentsState'].agents,
      filters: {...state['agentsState'].filters}
    };
    const {
      location,
      priceDownToUp,
      rating,
      numberOfRaters,
      search
    } = agentsStateResult.filters;

    if (location) {
      agentsStateResult.agents = this.filterByLocation(agentsStateResult.agents, location);
    }
    this.sortByPrice(agentsStateResult.agents, priceDownToUp);
    if (rating) {
      agentsStateResult.agents = this.sortByRating(agentsStateResult.agents, priceDownToUp);
    }
    if (numberOfRaters) {
      agentsStateResult.agents = this.sortByNumberRaters(agentsStateResult.agents, priceDownToUp);
    }
    if (!StringUtils.isEmpty(search)) {
      agentsStateResult.agents = this.filterBySearch(agentsStateResult.agents, search);
    }
    return <Agent[]>agentsStateResult.agents;
  }

  public static filterByLocation(agentList: Agent[], location: string): Agent[] {
    return agentList.filter(
      agent => agent.location.area === location);
  }

  public static sortByPrice(agentList: Agent[], priceDownToUp: boolean): void {
    agentList.sort(
      (a, b) => priceDownToUp ? a.price - b.price : b.price - a.price
    );
  }

  public static sortByRating(agentsList: Agent[], priceDownToUp: boolean): Agent[] {
    const sortedGroupsByPrice = this.groupSortByPrice(agentsList, priceDownToUp);
    sortedGroupsByPrice.map(agentsGroupByPrice =>
      agentsGroupByPrice.sort((a, b) => b.rating - a.rating));
    return sortedGroupsByPrice.concat.apply([], sortedGroupsByPrice);
  }

  public static sortByNumberRaters(agentsList: Agent[], priceDownToUp: boolean): Agent[] {
    const sortedGroupsByPrice = this.groupSortByPrice(agentsList, priceDownToUp);
    sortedGroupsByPrice.map(agentsGroupByPrice =>
      agentsGroupByPrice.sort((a, b) => b.numberOfRaters - a.numberOfRaters));
    return sortedGroupsByPrice.concat.apply([], sortedGroupsByPrice);
  }

  private static groupSortByPrice(agentsList: Agent[], priceDownToUp: boolean) {
    const groupByRating = groupBy(agentsList, 'price');
    const sortedGroupsByPrice = Object.assign([], groupByRating)
      .filter(a => a === a);
    if (!priceDownToUp) {
      sortedGroupsByPrice.reverse();
    }
    return sortedGroupsByPrice;
  }

  private static filterBySearch(agentsList: Agent[], search: string): Agent[] {
    return agentsList.filter((agent) => agent.name.includes(search));
  }
}
