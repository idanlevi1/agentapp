export class StringUtils {

  public static isEmpty(string: string): boolean {
    return !(string && (string.trim() !== ''));
  }

}
